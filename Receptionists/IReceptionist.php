<?php 

interface IReceptionist {
    public function helloVisiter();
    public function myNameIs();
    public function todayEvents();
    public function welcome();
}