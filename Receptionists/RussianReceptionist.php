<?php

class RussianReceptionist implements IReceptionist {

    public function helloVisiter(){
        echo "Добрый день!";
    }
    
    public function myNameIs(){
        echo "Меня зовут Евгений";
    }

    public function todayEvents(){
        echo "Сегодня у нас в программе:", PHP_EOL;
        echo " - Третьяковская галлерея", PHP_EOL;
        echo " - Большой театр", PHP_EOL;
        echo " - Крассная площадь";
    }

    public function welcome(){
        echo "Добро пожаловать!";
    }
}