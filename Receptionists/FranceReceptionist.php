<?php

require_once("./Receptionists/IReceptionist.php");

class FranceReceptionist implements IReceptionist {

    public function helloVisiter(){
        echo "Bonjour!";
    }

    public function myNameIs(){
        echo "je m'appelle Georges!";
    }

    public function todayEvents(){
        echo "Aujourd'hui dans notre programme:", PHP_EOL; 
        echo "- la vie paysanne", PHP_EOL;
        echo "- Le Tour de France", PHP_EOL;
        echo "- le camping", PHP_EOL;
        echo "- parcours sportif";    
    }

    public function welcome(){
        echo "accueillir!"; 
        
    }
}