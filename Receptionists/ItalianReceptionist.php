<?php

class ItalianReceptionist implements IReceptionist {

    public function helloVisiter(){
        echo "Buon pomeriggio!";
    }
    
    public function myNameIs(){
        echo "Il mio nome è Giorgio";
    }

    public function todayEvents(){
        echo "Oggi abbiamo in programma", PHP_EOL;
        echo " - Colliseo", PHP_EOL;
        echo " - Teatro Storky", PHP_EOL;
        echo " - Canale Grande";   
    }

    public function welcome(){
        echo "Ben arrivato";
    }
}