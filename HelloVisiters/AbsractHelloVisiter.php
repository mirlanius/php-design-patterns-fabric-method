<?php

abstract class AbsractHelloVisiter {
  
    public function Greatings(){
        $receptionist = $this->getReceptionist();

        $receptionist->helloVisiter();
        echo PHP_EOL;
        $receptionist->myNameIs();
        echo PHP_EOL;
        $receptionist->todayEvents();
        echo PHP_EOL;
        $receptionist->welcome();
        echo PHP_EOL;
        echo PHP_EOL;

    }

    abstract public  function getReceptionist():IReceptionist;


}