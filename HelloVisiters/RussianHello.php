<?php
require_once("./HelloVisiters/AbsractHelloVisiter.php");
require_once("./Receptionists/RussianReceptionist.php");

class RussianHello extends AbsractHelloVisiter {

    public function getReceptionist():IReceptionist{
        return new RussianReceptionist();
    }

}