<?php
require_once("./HelloVisiters/AbsractHelloVisiter.php");
require_once("./Receptionists/FranceReceptionist.php");

class FrenchHello extends AbsractHelloVisiter {

    public function getReceptionist():IReceptionist{
        return new FranceReceptionist();
    }

}