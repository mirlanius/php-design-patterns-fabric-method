<?php
require_once("./HelloVisiters/AbsractHelloVisiter.php");
require_once("./Receptionists/ItalianReceptionist.php");

class ItalianHello extends AbsractHelloVisiter {

    public function getReceptionist():IReceptionist{
        return new ItalianReceptionist();
    }

}